module Input_Eclipse_module

!  Module of routines which read industry-standard Eclipse files
!  Eclipse is a trademark of the Schlumberger Corporation

#include "petsc/finclude/petscsys.h"
  use petscsys
  use PFLOTRAN_Constants_module

  implicit none

  public :: ReadEclipseRestart

contains

! ************************************************************************** !

subroutine ReadEclipseRestart(realization_base,option)
  !
  ! Read an Eclipse restart file and reset the Pflotran solution
  !
  ! Author: Dave Ponting
  ! Date: 10/09/19

  use Option_module
  use Output_Eclipse_module, only : ReadEclipseRestartFile
  use Field_module
  use Grid_module
  use Patch_module
  use Realization_Base_class, only : realization_base_type
  use Discretization_module
  use IO_Eclipse_Util_module

  implicit none

  class(realization_base_type), target :: realization_base
  type(option_type), intent(in), pointer :: option

  character(len=MAXSTRINGLENGTH) :: efilename
  PetscReal :: timereq, timeact

  type(grid_type), pointer  :: grid
  type(patch_type), pointer :: patch
  type(field_type), pointer :: field

  PetscInt :: ierr, nlmax, ephase

  PetscReal, pointer        :: vsoll(:,:)
  PetscInt                  :: nsol
  character(len=8), pointer :: zsol(:)

  PetscBool, parameter :: store_den = PETSC_FALSE

  ! Initialisation

  ierr = 0
  nsol = 0
  ephase = 0

  efilename = option%restart_filename
  timereq   = option%restart_time

  grid => realization_base%patch%grid
  patch => realization_base%patch
  field => realization_base%field

  nlmax = grid%nlmax

  ! Set up the Eclipse restart maps

  call setupEwriterRestMaps(patch, grid, option)

  ! Allocate stack of solutions to hold data from restart file

  call allocateLocalSolution(vsoll, nsol, zsol, nlmax, option, store_den)

  !  Read the Eclipse restart file

  call ReadEclipseRestartFile(efilename,timereq,vsoll, nsol, zsol, &
                              option, timeact)
  option%e_restart_time = timeact
  realization_base%output_option%plot_number = 1

  ! Unload the stack of solutions into xx_loc_p

  call UnloadLocalSolution(vsoll, nsol, zsol, option, realization_base)

  ! Delete the stack as no longer required

  call deleteLocalSolution(vsoll, zsol)

  ! Globalise the local solutin that has just been over-written

  call DiscretizationLocalToGlobal(realization_base%discretization, &
                                   field%flow_xx_loc, &
                                   field%flow_xx    , &
                                   NFLOWDOF)

  ! Copy flow_xx globals to flow_yy globals

  call VecCopy(field%flow_xx, field%flow_yy, ierr)
  CHKERRQ(ierr)

end subroutine ReadEclipseRestart


! ************************************************************************** !

subroutine UnloadLocalSolution(vsoll, nsol, zsol, option, realization_base)
  !
  ! Unload solution read from Eclipse restart files
  !
  ! Author: Dave Ponting
  ! Date  : 10/10/19

  use Option_module
  use TOWG_module
  use TOilIms_module
  use Realization_Subsurface_class
  use Realization_Base_class, only : realization_base_type

  implicit none

  class(realization_base_type), target :: realization_base

  PetscReal, pointer        :: vsoll(:,:)
  PetscInt                  :: nsol
  character(len=8), pointer :: zsol(:)
  type(option_type) :: option

  !  Loop over required solutions

  select type (realization_base)
    class is (realization_subsurface_type)
    select case(option%iflowmode)
      case(TOWG_MODE)
        call TOWGLoad(realization_base,vsoll,zsol,nsol)
      case(TOIL_IMS_MODE) 
        call TOilImsLoad(realization_base,vsoll,zsol,nsol)
    end select
  end select

end subroutine UnloadLocalSolution

end module Input_Eclipse_module
